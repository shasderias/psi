package main

import "fmt"
import "time"
import "net/http"

// https://gobyexample.com/time-formatting-parsing
// fmt.Fprintln(w, t.Format(layout))
func formattime(w http.ResponseWriter, r *http.Request) {
	// p := fmt.Println
	p := fmt.Fprintln
	t := time.Now()
	p(w, "(1) ", t.Format(time.RFC3339))

	t1, e := time.Parse(
		time.RFC3339,
		"2012-11-01 22:08:41+00:00") // output 0001-01-01 00:00:00 +0000 UTC??
	p(w, "(2) ", t1)

	p(w, "(3) ", t.Format("3:04PM"))
	p(w, "(4) ", t.Format("Mon Jan _2 15:04:05 2007")) // change year from 2006 to 2007
	p(w, "(5) ", t.Format("2006-01-02 15:04:05.999999-07:00"))
	form := "3 04 PM"
	t2, e := time.Parse(form, "8 41 PM")
	p(w, "(6) ", t2)

	fmt.Fprintf(w, "(7) %d-%02d-%02d T %02d:%02d:%02d-00:00\n",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	ansic := "Mon Jan _2 15:04:05 2006"
	_, e = time.Parse(ansic, "8:41PM")
	p(w, "(8) ", e)

	// the output
	// 	(1)  2018-04-26T20:31:03+08:00
	// (2)  0001-01-01 00:00:00 +0000 UTC
	// (3)  8:31PM
	// (4)  Thu Apr 26 20:31:03 2018
	// (5)  2018-04-26 20:31:03.069325+08:00
	// (6)  0000-01-01 20:41:00 +0000 UTC
	// (7) 2018-04-26 T 20:31:03-00:00
	// (8)  parsing time "8:41PM" as "Mon Jan _2 15:04:05 2006": cannot parse "8:41PM" as "Mon"
}
